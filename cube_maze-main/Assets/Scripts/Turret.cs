using System.Threading.Tasks;
using UnityEngine;

public class Turret : MonoBehaviour
{
    [Header("Observable")]
    [SerializeField] private float _coolDown;

    [Header("Object")] 
    [SerializeField] private GameObject _spawner;
    
    private float _timer = 0f;
    void Update()
    {
        Timer();
    }

    private void Timer()
    {
        _timer += Time.deltaTime;
        if (_timer >= _coolDown)
        {
            Shot();
            _timer = 0f;
        }
    }

    private async void Shot()
    {
        if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, 100f))
            if (hit.transform.tag.Contains("Player"))
            {
                await Task.Delay(200);
                hit.transform.GetComponent<Player>().Kill();
            }
    }
}
